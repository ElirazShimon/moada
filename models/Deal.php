<?php

namespace app\models;

use Yii;
use app\models\Lead;

/**
 * This is the model class for table "deal".
 *
 * @property integer $id
 * @property integer $name
 * @property integer $leadid
 * @property integer $amount
 */
class Deal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name', 'leadid', 'amount'], 'required'],
            [['id', 'name', 'leadid', 'amount'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'leadid' => 'Leadid',
            'amount' => 'Amount',
        ];
    }
	public function getLead()
   {

        return $this->hasOne(Lead::className(), ['id' => 'leadid']);
    }
}
